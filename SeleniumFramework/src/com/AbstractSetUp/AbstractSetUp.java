package com.AbstractSetUp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;


public class AbstractSetUp {

	public static Logger Add_Log = null;
	public WebDriver driver;
	public SoftAssert s_assert = new SoftAssert();


	public static Properties Param = null;
	public String parentHandle = null;
	public String baseURL = null;
	protected int DEFAULT_WAIT_TIME;

	//Declaration for system property in getting the absolute path of the file
	public static final String Path_TestData = System.getProperty("user.dir")+"//src//com//Files//";
	public static final String File_TestData = "SampleExcel.xlsx";

	//Initializing the logger and properties file
	public void init() throws Exception{
		Add_Log = Logger.getLogger("rootLogger");

		Param = new Properties();
		FileInputStream file = new FileInputStream(System.getProperty("user.dir")+"//src//com//Property//param.properties");
		Param.load(file);

		baseURL = Param.getProperty("baseURL");
		DEFAULT_WAIT_TIME = Integer.parseInt(Param.getProperty("DEFAULT_WAIT_TIME"));
		Add_Log.info("Param.properties file loaded successfully.");

	}

	//This is the setup for the browser to launch into maximize
	@BeforeClass
	@Parameters( {"BROWSER"})
	public void setUp(String BROWSER) throws Exception{
		init();
		if(BROWSER.equalsIgnoreCase("FIREFOX"))
		{
			//To Load Firefox Driver
			driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			parentHandle = driver.getWindowHandle(); 
			Add_Log.info("Firefox Driver Instance was loades successfully");
		} else if (BROWSER.equalsIgnoreCase("chrome"))
		{
			//To Load Chrome driver Instance.
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src//BrowserDrivers//chromedriver.exe");
			driver = new ChromeDriver();
			Add_Log.info("Chrome Driver Instance loaded successfully.");
			parentHandle = driver.getWindowHandle(); // get the current window handle
		}
	}


	@AfterMethod 
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException 
	{ 
		//If after @Test there will be error found, it will take a screenshot
		if (testResult.getStatus() == ITestResult.FAILURE) 
		{  
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//Screenshots//testfailScreenShot_"+dateFormat.format(new Date()) + ".png")); 
		} 

	} 


	@AfterClass
	public void tearDown()throws IOException{
		//Close and quit all web browser open
		driver.close();
		driver.quit();
	}


	//Enter and Clear options in sending data
	public void sendData(String sendkeys, By by){
		driver.findElement(by).clear();
		driver.findElement(by).sendKeys(sendkeys);
	}


	//switch to child windows
	public void switchChildWindow(){

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (newly opened window)
		}
	}

	//getElementByXPath function for static xpath
	public WebElement getElementByXPath(String Key){
		try{
			//This block will find element using Key value from web page and return It.
			return driver.findElement(By.xpath(Key));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//getElementByXPath function for dynamic xpath
	public WebElement getElementByXPath(String Key1, int val, String key2){
		try{
			//This block will find element using values of Key1, val and key2 from web page and return It.
			return driver.findElement(By.xpath(Key1+val+key2));
		}catch(Throwable t){
			//If element not found on page then It will return null.
			Add_Log.info("Object not found for custom xpath");
			return null;
		}
	}

	//Call this function to locate element by ID locator.
	public WebElement getElementByID(String Key){
		try{
			return driver.findElement(By.id(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}

	}

	//Call this function to locate element by Name Locator.
	public WebElement getElementByName(String Key){
		try{
			return driver.findElement(By.name(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//Call this function to locate element by cssSelector Locator.
	public WebElement getElementByCSS(String Key){
		try{
			return driver.findElement(By.cssSelector(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//Call this function to locate element by ClassName Locator.
	public WebElement getElementByClass(String Key){
		try{
			return driver.findElement(By.className(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//Call this function to locate element by tagName Locator.
	public WebElement getElementByTagName(String Key){
		try{
			return driver.findElement(By.tagName(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//Call this function to locate element by link text Locator.
	public WebElement getElementBylinkText(String Key){
		try{
			return driver.findElement(By.linkText(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	//Call this function to locate element by partial link text Locator.
	public WebElement getElementBypLinkText(String Key){
		try{
			return driver.findElement(By.partialLinkText(Key));
		}catch(Throwable t){
			Add_Log.info("Object not found for key --"+Key);
			return null;
		}
	}

	///////////////////////
	public boolean isLinkPresent(String link ) throws Exception
	{
		try 
		{ 
			if (driver.findElement(By.linkText(link)).isDisplayed()) 
			{
				return true; 
			}
		} catch (NoSuchElementException e){} 
		return false;
	}

	public boolean isTextPresent(String text ) throws Exception
	{
		try 
		{ 
			if (driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*"+text+"[\\s\\S]*$"))
			{
				return true; 
			}
		} catch (NoSuchElementException e){}
		return false;
	}

	public boolean isElementPresent(By by) throws Exception
	{
		try {
			driver.findElement(by).isDisplayed();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}  
	}

	public void inputString(By element, String input)
	{
		driver.findElement(element).clear();
		driver.findElement(element).sendKeys(input);
	}

	public void clickRadioButton(String btnName, String btnIndex)
	{
		driver.findElement(By.id(btnName + btnIndex)).click();
	}

	public void selectDropDown(By by, String optionText)
	{
		WebElement element = driver.findElement(by);
		Select selectData=new Select(element);
		selectData.selectByVisibleText(optionText);  
	}

	public void clickButtonWithValue( String text)
	{
		driver.findElement(By.xpath("//input[@value='" + text + "']")).click();
	}

	//Call this function for Waiting an element
	public void waitForElementToLoad(By by)
	{
		WebDriverWait wait = new WebDriverWait(driver,DEFAULT_WAIT_TIME);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	//Call this function for Waiting a page to load
	public void waitForPageToLoad()
	{ 
		(new WebDriverWait(driver, DEFAULT_WAIT_TIME)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				return (((org.openqa.selenium.JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
			}
		});
	}

	public void hoverAndClick(By by)
	{
		WebElement element = driver.findElement(by);
		Point p = element.getLocation();
		((JavascriptExecutor) driver).executeScript("window.scrollTo(" + p.x + "," + (p.y - 100) + ");");
		driver.findElement(by).click();
	}   

	public void ZoomIn()
	{
		((JavascriptExecutor)driver).executeScript("document.body.style.zoom='67%'");

	}   

	public void scrollPage() throws InterruptedException
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("javascript:window.scrollTo(100,100)");
		Thread.sleep(500); 
	} 

	public void hoverClick(By by)
	{
		WebElement element = driver.findElement(by);
		Point p = element.getLocation();
		((JavascriptExecutor) driver).executeScript("window.scrollTo(" + p.x + "," + (p.y - 100) + ");");
		driver.findElement(by).click();
	}

}
